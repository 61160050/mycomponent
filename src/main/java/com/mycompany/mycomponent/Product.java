/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    @Override
    public String toString() {
        return "ID: "+id+" Name: "+name+" Price: "+price+" Image: "+ image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"เอสเพรสโซ่ 1",40,"./image/1.jpg"));
        list.add(new Product(2,"เอสเพรสโซ่ 2",30,"./image/1.jpg"));
        list.add(new Product(3,"เอสเพรสโซ่ 3",40,"./image/1.jpg"));
        list.add(new Product(4,"อเมริกาโน่ 1",30,"./image/2.jpg"));
        list.add(new Product(5,"อเมริกาโน่ 2",40,"./image/2.jpg"));
        list.add(new Product(6,"ชาเย็น 1",50,"./image/3.jpg"));
        list.add(new Product(7,"ชาเย็น 2",40,"./image/3.jpg"));
        return list;
    }
}
